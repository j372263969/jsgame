<?php
ini_set("display_errors", "On");
error_reporting(E_ALL | E_STRICT);

class JSSDK {
  private $jsapiTicket;
  private $appId;
  private $url;

  public function __construct() {
    $this->url = "http://www.witprobe.com";
    $mysqli = new mysqli('localhost:3306', 'root', 'j13517866888');
    $result = $mysqli->query('SELECT `key` `value` from `weixin`.`weixin_config` WHERE `key` = \'jsapi_ticket\'');

    while($row = $result->fetch_array()) {
      $this->jsapiTicket = $row['value'];
    }
    $result->free();

    $result = $mysqli->query('SELECT `key` `value` from `weixin`.`weixin_config` WHERE `key` = \'app_id\'');

    while($row = $result->fetch_array()) {
      $this->appId = $row['value'];
    }
    $result->free();
    $mysqli->close();
  }

  public function getSignPackage() {
    $url = $this->url;
    $jsapiTicket = $this->jsapiTicket;
    $timestamp = time();
    $nonceStr = $this->createNonceStr();

    // 这里参数的顺序要按照 key 值 ASCII 码升序排序
    $string = "jsapi_ticket=".$jsapiTicket."&noncestr=".$nonceStr."&timestamp=".$timestamp."&url=".$url;

    $signature = sha1($string);

    $signPackage = array(
      "appId"     => $this->appId,
      "nonceStr"  => $nonceStr,
      "timestamp" => $timestamp,
      "signature" => $signature,
      "rawString" => $string
    );
    return $signPackage; 
  }

  private function createNonceStr($length = 16) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $str = "";
    for ($i = 0; $i < $length; $i++) {
      $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
    }
    return $str;
  }
}

$signer = new JSSDK();

echo json_encode($signer->getSignPackage());