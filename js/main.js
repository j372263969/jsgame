require.config({
	baseUrl: 'js',
	paths: {
		app: 'app',

		GameMgr: 'app/util/GameMgr',
		tool: 'app/util/tool',
		GameTicker: 'app/util/GameTicker',

		Circle: 'app/gameobject/Circle',
		BackImage: 'app/gameobject/BackImage',
		Button: 'app/gameobject/Button',
		Trap: 'app/gameobject/Trap',
		TrapGenerator: 'app/gameobject/TrapGenerator',
		ScoreText: 'app/gameobject/ScoreText',

		resources: 'app/data/resources',

		createjs: 'lib/easeljs-0.8.2.min',
		preload: 'lib/preloadjs-0.6.2.min',
		tween: 'lib/tweenjs-0.6.2.min',
		webgl: 'lib/webgl-0.8.2.min',
		jQuery: 'lib/jquery-2.1.4.min',
	},
	shim: {
		preload: ['createjs'],
		webgl: ['createjs'],
		tween: ['createjs'],
		createjs: {
			exports: 'createjs'
		},
		jQuery: {
			exports: 'jQuery'
		}
	}
});

require(['jQuery', 'app'], function ($, app) {
	$(app.init);
});