define([], function(){
	return [
		{
			key: 'back',
			path: 'image/back.jpg'
		},
		{
			key: 'circle',
			path: 'image/circle.png'
		},
		{
			key: 'startBtn',
			path: 'image/startBtn.png'
		},
		{
			key: 'trap',
			path: 'image/trap.png'
		},
		{
			key: 'restartBtn',
			path: 'image/restartBtn.png'
		}
	];
});