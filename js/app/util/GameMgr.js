define(['createjs', 'Button', 'preload', 'tween'],function(createjs, Button, tool) {
	var GameMgr = {
		cooHeight : 1500,
		cooWidth : 900,
		loadQueue : new createjs.LoadQueue(false),
		resources : [],
		score : 0,
	};

	GameMgr.isOver = false;
	GameMgr.trapGenerator = {};

	GameMgr.restart = function() {
		var app = require('app');
		GameMgr.reset();
		app.initGameObject();
		app.run();
	};

	GameMgr.gameOver = function() {
		GameMgr.trapGenerator.isEnd = true;
		GameMgr.circle.isEnd = true;
		setTimeout(function() {
			var tool = require('tool');
			var restartBtn = new Button(GameMgr.getResources('restartBtn'));
			tool.bitmap.setSize(restartBtn, 400, 400);
			var bounds = restartBtn.getBounds();
			restartBtn.x = (GameMgr.cooWidth * GameMgr.scaleFactor - bounds.width) / 2;
			restartBtn.y = (GameMgr.cooHeight * GameMgr.scaleFactor - bounds.height) / 2;
			GameMgr.stage.addChild(restartBtn);
			restartBtn.on('click', function(){
				GameMgr.stage.removeChild(restartBtn);
				GameMgr.isOver = true;
			}, 500);
		});
	};

	GameMgr.reset = function() {
		createjs.Ticker.reset();
		GameMgr.gameTicker.removeAllRightNow();
		GameMgr.stage.removeAllEventListeners();
		GameMgr.stage.removeAllChildren();
		GameMgr.score = 0;
		GameMgr.circle = null;
		GameMgr.scoreText = null;
		GameMgr.isOver = false;
	};

	GameMgr.loadResources = function(list, callback) {
		this.loadQueue.addEventListener('complete', callback);
		this.loadQueue.addEventListener('fileload', function(event) {
			this.resources[event.item.id] = event.result;
		}.bind(this));
		for(var i in list) {
			this.loadQueue.loadFile({id: list[i].key, src: list[i].path});
		}
		this.loadQueue.load();
	};

	GameMgr.getResources = function(key) {
		return this.resources[key];
	};

	GameMgr.initScaleFactor = function(width, height) {
		var screenRatio = this.cooWidth / this.cooHeight;
		if(width / this.cooWidth > height / this.cooHeight) {
			this.scaleFactor = width / this.cooWidth;
		} else {
			this.scaleFactor = height / this.cooHeight;
		}
	};

	GameMgr.addScore = function(num) {
		this.score += num;
		this.scoreText.text = this.score + "";
	};

	return GameMgr;
});