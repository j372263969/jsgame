define(['GameMgr'], function(GameMgr) {
	var tool = {};
	tool.bitmap = {};
	tool.bitmap.setSize = function(bitmap, w, h) {
		var height = bitmap.image.height;
		var width = bitmap.image.width;
		bitmap.scaleX = w * GameMgr.scaleFactor * 1.0 / width;
		bitmap.scaleY = h * GameMgr.scaleFactor * 1.0 / height;
		bitmap.setBounds(0, 0, w * GameMgr.scaleFactor, h * GameMgr.scaleFactor);
	};

	tool.isInterRect = function(bitmap1, bitmap2) {
		var bounds1 = bitmap1.getBounds();
		var bounds2 = bitmap2.getBounds();
		var b1x1 = bitmap1.x;
		var b1x2 = bitmap1.x + bounds1.width;
		var b1y1 = bitmap1.y;
		var b1y2 = bitmap1.y + bounds1.height;
		
		var b2x1 = bitmap2.x;
		var b2x2 = bitmap2.x + bounds2.width;
		var b2y1 = bitmap2.y;
		var b2y2 = bitmap2.y + bounds2.height;

		var flag1 = (b1x1 - b2x1) * (b1x1 - b2x2);
		var flag2 = (b1x2 - b2x1) * (b1x2 - b2x2);
		var flagx = (flag1 <= 0) || (flag2 <= 0);

		var flag3 = (b1y1 - b2y1) * (b1y1 - b2y2);
		var flag4 = (b1y2 - b2y1) * (b1y2 - b2y2);
		var flagy = (flag3 <=0 ) || (flag4 <=0 );


		return flagx && flagy;

	};

	tool.isInterCircle = function(bitmap1, bitmap2) {
		var bounds1 = bitmap1.getBounds();
		var bounds2 = bitmap2.getBounds();
		var r1 = (bounds1.height + bounds1.width) / 4;
		var r2 = (bounds2.height + bounds2.width) / 4;
		var deltaX = (bitmap1.x + r1) - (bitmap2.x + r2);
		var deltaY = (bitmap1.y + r1) - (bitmap2.y + r2);
		return (deltaX * deltaX) + (deltaY * deltaY) <= (r1 + r2) * (r1 + r2);
	};


	tool.HashTable = function() {
	    var size = 0;
	    var entry = {};
	    this.add = function (key, value) {
	        if (!this.containsKey(key)) {
	            size++;
	        }
	        entry[key] = value;
	    };
	    this.getValue = function (key) {
	        return this.containsKey(key) ? entry[key] : null;
	    };
	    this.remove = function (key) {
	        if (this.containsKey(key) && (delete entry[key])) {
	            size--;
	        }
	    };
	    this.containsKey = function (key) {
	        return (key in entry);
	    };
	    this.containsValue = function (value) {
	        for (var prop in entry) {
	            if (entry[prop] == value) {
	                return true;
	            }
	        }
	        return false;
	    };
	    this.getValues = function () {
	        var values = [];
	        for (var prop in entry) {
	            values.push(entry[prop]);
	        }
	        return values;
	    };
	    this.getKeys = function () {
	        var keys = [];
	        for (var prop in entry) {
	            keys.push(prop);
	        }
	        return keys;
	    };
	    this.getSize = function () {
	        return size;
	    };
	    this.clear = function () {
	        size = 0;
	        entry = {};
	    };
	};
	
	return tool;
});