define(['GameMgr', 'createjs', 'tool'],function(GameMgr, createjs, tool){
	function GameTicker(stage) {
		this.updates = new tool.HashTable();
		this.waitToRemove = [];
		this.tick = this.tick.bind(this);
		this.stage = stage;
		this.addUpdate = this.addUpdate.bind(this);
		this.isRemoveAll = false;
		this.lastUpdateId = 0;
	}

	GameTicker.prototype.addUpdate = function(update) {
		this.lastUpdateId++;
		this.updates.add(this.lastUpdateId, update);
		return this.lastUpdateId;
	};

	GameTicker.prototype.removeUpdate = function(id) {
		this.waitToRemove.push(id);
	};

	GameTicker.prototype.removeProcess = function() {
		if(this.isRemoveAll) {
			this.updates.clear();
		} else {
			for(var i = 0; i < this.waitToRemove.length ; i++) {
				this.updates.remove(this.waitToRemove[i]);
			}
		}
		this.waitToRemove = [];
		this.isRemoveAll = false;
	};

	GameTicker.prototype.removeAll = function() {
		this.isRemoveAll = true;
	};

	GameTicker.prototype.removeAllRightNow = function() {
		this.removeAll();
		this.removeProcess();
	};

	GameTicker.prototype.tick = function(event) {
		this.event = event;
		var updateValues = this.updates.getValues();
		for(var i in updateValues) {
			if(GameMgr.isOver) {
				break;
			}
			updateValues[i]();
		}

		this.removeProcess();

		if(GameMgr.isOver) {
			createjs.Ticker.removeEventListener(this.tick);
			setTimeout(GameMgr.restart(), 0);
		} else {
			this.stage.update();
		}
	};

	return GameTicker;
});