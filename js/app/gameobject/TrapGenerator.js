define(['Trap', 'GameMgr'], function(Trap, GameMgr) {
	//Every trap generated has a callback
	function TrapGenerator(callback){
		this.isEnd = false;
		this.period = 3000;
		this.periodFix = 930;
		this.nextTime = this.period;
		this.timer = this.period;
		this.callback = callback;
		this.update = this.update.bind(this);
		this.count = 0;
	}

	TrapGenerator.prototype.update = function() {
		if(GameMgr.isOver || this.isEnd)
			return;
		this.timer += GameMgr.gameTicker.event.delta;
		if(this.timer >= this.nextTime) {
			this.timer -= this.nextTime;
			this.count ++;
			increaseHard = GameMgr.score * 25;
			if(increaseHard > 520)
				increaseHard = 520;
			GameMgr.circle.gravity = 0.0028 + increaseHard / 30500;
			GameMgr.circle.calculateJump();
			this.nextTime = this.period + this.periodFix * Math.random() - increaseHard * 2;
			this.callback(this.product());
		}
	};

	TrapGenerator.prototype.product = function() {
		var maxLeft = (Math.random() * 120 + 50);
		var maxRight = (Math.random() * 120 + 730);
		var km = (Math.random() * 0.000020 + 0.000004);
		var upSpeed = 0.33 + GameMgr.score * 0.005;
		if(upSpeed > 0.55)
			upSpeed = 0.55;

		return new Trap(maxLeft, maxRight, upSpeed, km);
	};

	return TrapGenerator;
});