define(['createjs', 'tool', 'GameMgr'], function(createjs, tool, GameMgr) {
	function Trap(maxLeft, maxRight, upSpeed, km) {
		this.Bitmap_constructor(GameMgr.getResources('trap'));
		this.upSpeed = upSpeed;
		this.isEnd = false;
		this.addScore = false;
		this.upSpeed = upSpeed;
		this.radius = 70;
		this.maxRight = maxRight;
		this.maxLeft = maxLeft;
		this.hoverCenter = (maxLeft + maxRight) / 2;
		this.A = (maxRight - maxLeft) / 2;
		this.km = km;
		this.xSpeed = 0;

		this.hover = this.hover.bind(this);
		this.update = this.update.bind(this);
		this.resize = this.resize.bind(this);
		this.attack = this.attack.bind(this);
		tool.bitmap.setSize(this, this.radius * 2, this.radius * 2);

		this.changSize = false;
		if(Math.random() < 0.3) {
			this.changSize = true;
			this.maxRadius = Math.random() * 40 + 80;
			this.minRadius = Math.random() * 15 + 30;
			this.sizeSpeed = Math.random() * 0.05 + 0.03;
			this.changWay = 1;
		}

		if(Math.random() < 0.5) {
			this.x = (maxRight) * GameMgr.scaleFactor;
		} else {
			this.x = (maxLeft) * GameMgr.scaleFactor;
		}


	}
	createjs.extend(Trap, createjs.Bitmap);
	createjs.promote(Trap, 'Bitmap');

	Trap.prototype.hover = function() {
		this.xSpeed += (this.hoverCenter - this.x / GameMgr.scaleFactor) * this.km * GameMgr.gameTicker.event.delta;
		this.x += this.xSpeed * GameMgr.gameTicker.event.delta * GameMgr.scaleFactor;
	};

	Trap.prototype.resize = function() {
		if(!this.changSize)
			return;
		if(this.changWay === 1) {
			this.radius += this.sizeSpeed * GameMgr.gameTicker.event.delta;
			if(this.radius > this.maxRadius) {
				this.radius = this.maxRadius;
				this.changWay = 0;
			}
		}
		if(this.changWay === 0) {
			this.radius -= this.sizeSpeed * GameMgr.gameTicker.event.delta;
			if(this.radius < this.minRadius) {
				this.radius = this.minRadius;
				this.changWay = 1;
			}
		}
		tool.bitmap.setSize(this, this.radius * 2, this.radius * 2);
	};

	Trap.prototype.attack = function(){
		if(GameMgr.circle.isEnd)
			return;
		if(tool.isInterCircle(this, GameMgr.circle)) {
			this.isEnd = true;
			GameMgr.gameOver();
		}
	};

	Trap.prototype.update = function() {
		if(this.isEnd) {
			if(this.alpha > 0)
				this.alpha -= 0.002 * GameMgr.gameTicker.event.delta;
			return;
		}
		this.y -= this.upSpeed * GameMgr.gameTicker.event.delta * GameMgr.scaleFactor;
		if(this.y < 0 - this.radius * 2 * GameMgr.scaleFactor) {
			this.isEnd = true;
			GameMgr.gameTicker.removeUpdate(this.updateId);
			GameMgr.stage.removeChild(this);
		}
		var circle  = GameMgr.circle;
		if(circle.y > this.y + GameMgr.scaleFactor * 2 * this.radius && !this.addScore && !circle.isEnd) {
			GameMgr.addScore(1);
			this.addScore = true;
		}

		this.hover();
		this.resize();
		this.attack();
	};
	return Trap;
});