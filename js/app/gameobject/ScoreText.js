define(['createjs', 'GameMgr'],function(createjs, GameMgr){

	function ScoreText() {
		this.Text_constructor("0", 285 * GameMgr.scaleFactor +"px Helvetica", "#00FF00");
		this.goAlpha = this.alpha;
		this.goAlphaSpeed = 0.002;
		this.update = this.update.bind(this);

		this.y = 145 * GameMgr.scaleFactor;
		this.alpha = 0;
		this.goAlapha = 1;
	}

	createjs.extend(ScoreText, createjs.Text);
	createjs.promote(ScoreText, 'Text');

	ScoreText.prototype.update = function() {
		this.x = (GameMgr.cooWidth / 2 - this.getBounds().width / 2) * GameMgr.scaleFactor;
		if(this.alpha < this.goAlpha) {
			this.alpha += this.goAlphaSpeed * GameMgr.gameTicker.event.delta;
			if(this.alpha > 1)
				this.alpha = 1;
		}
		if(this.alpha > this.goAlpha) {
			this.alpha -= this.goAlphaSpeed * GameMgr.gameTicker.event.delta;
			if(this.alpha < 0)
				this.alpha = 0;
		}
	};

	return ScoreText;
});
