define(['createjs'], function (createjs){
	function Button(image) {
		this.Bitmap_constructor(image);
	}
	createjs.extend(Button, createjs.Bitmap);
	createjs.promote(Button, 'Bitmap');
	return Button;
});