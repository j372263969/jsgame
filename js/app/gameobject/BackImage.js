define(['createjs', 'GameMgr'], function(createjs, GameMgr) {
	function BackImage() {
		this.Bitmap_constructor(GameMgr.getResources('back'));
		this.x = 0;
		this.y = 0;
		this.speed = 0.15;
		this.update = this.update.bind(this);
		suitStage(this);
		this.bounds = {
			width: this.image.width * this.scaleX,
			height: this.image.height * this.scaleY
		};


	}

	createjs.extend(BackImage, createjs.Bitmap);
	createjs.promote(BackImage, "Bitmap");

	BackImage.prototype.update = function() {
		var event = GameMgr.gameTicker.event;
		this.y -= this.speed * event.delta * GameMgr.scaleFactor;
		if(this.y <= -(this.bounds.height - GameMgr.stage.canvas.height)) {
			this.y += this.bounds.height * 0.5;
		}
	};

	function suitStage(thing) {
		var width = thing.image.width;
		var height = thing.image.height;
		thing.scaleX = GameMgr.stage.canvas.width / 1.0 / width;
		thing.scaleY = thing.scaleX;
	}

	return BackImage;
});