define(['createjs', 'GameMgr', 'tool', 'tween'], function(createjs, GameMgr, tool) {
	function Circle() {
		var image = GameMgr.getResources('circle');
		this.Bitmap_constructor(image);
		this.radius = 75;
		tool.bitmap.setSize(this, this.radius * 2, this.radius * 2);
		this.stage = GameMgr.stage;
		this.x = (GameMgr.cooWidth / 2 - this.radius) * GameMgr.scaleFactor;
		this.y = 100 * GameMgr.scaleFactor;
		this.gravity = 0.0028;
		this.jumpV = 0.8;
		this.calculateJump = this.calculateJump.bind(this);
		this.calculateJump();
		this.update = this.update.bind(this);
		this.speed = 0;
		this.maxSpeed = 3;
		this.isEnd = false;

		GameMgr.stage.addEventListener("mousedown", function() {
			this.speed = -this.jumpV;
		}.bind(this));
	}

	createjs.extend(Circle, createjs.Bitmap);
	createjs.promote(Circle, 'Bitmap');

	Circle.prototype.calculateJump = function() {
		this.jumpV = Math.sqrt(this.gravity * 360);
	};

	Circle.prototype.update = function() {
		if(this.isEnd) {
			if(this.alpha > 0)
				this.alpha -= 0.002 * GameMgr.gameTicker.event.delta;
			return;
		}
		var event = GameMgr.gameTicker.event;
		this.speed += this.gravity * event.delta;
		this.y += this.speed * event.delta * GameMgr.scaleFactor;

		if(this.y > GameMgr.stage.canvas.height) {
			this.isEnd = true;
			GameMgr.gameOver();
		}

		if(this.y < 0 - 2 * this.radius * GameMgr.scaleFactor) {
			this.isEnd = true;
			GameMgr.gameOver();
		}

		if(this.speed > this.maxSpeed) {
			this.speed = this.maxSpeed;
		}
	};

	return Circle;
});