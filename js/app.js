define(['createjs',
		'GameTicker',
		'Circle',
		'GameMgr',
		'BackImage',
		'resources',
		'jQuery',
		'Button',
		'tool',
		'TrapGenerator',
		'ScoreText',
		'webgl',
		],
		function(
		createjs,
		GameTicker,
		Circle,
		GameMgr,
		BackImage,
		resources,
		$,
		Button,
		tool,
		TrapGenerator,
		ScoreText){

	function init() {
		$.ajax({
			type: 'POST',
			url: 'server/server.php'
		});
		GameMgr.loadResources(resources, function() {
			initStage();
			initGameObject();
			run();
		});
	}

	function initStage() {
		var stage;
		var gameTicker;
		var gameView;
		
		gameView = $('#gameView');		
		GameMgr.initScaleFactor($(window).width(), $(window).height());
		gameView.attr('width', GameMgr.cooWidth * GameMgr.scaleFactor);
		gameView.attr('height', GameMgr.cooHeight * GameMgr.scaleFactor);

		stage = new createjs.Stage("gameView", false, false);
		createjs.Touch.enable(stage);
		
		gameTicker = new GameTicker(stage);

		GameMgr.stage = stage;
		GameMgr.gameTicker = gameTicker;
	}

	function initGameObject() {
		var back = new BackImage();
		GameMgr.stage.addChild(back);
		back.updateId = GameMgr.gameTicker.addUpdate(back.update);

		var startButton = new Button(GameMgr.getResources('startBtn'));
		tool.bitmap.setSize(startButton, 330, 180);
		var bounds = startButton.getBounds();
		startButton.x = (GameMgr.cooWidth * GameMgr.scaleFactor - bounds.width) / 2;
		startButton.y = (GameMgr.cooHeight * GameMgr.scaleFactor - bounds.height) / 2;
		GameMgr.stage.addChild(startButton);

		startButton.on('click', function(){
			GameMgr.stage.removeChild(startButton);
			var circle = new Circle();
			circle.updateId = GameMgr.gameTicker.addUpdate(circle.update);
			GameMgr.stage.addChild(circle);
			GameMgr.circle = circle;

			var scoreText = new ScoreText();
			GameMgr.stage.addChild(scoreText);
			scoreText.updateId = GameMgr.gameTicker.addUpdate(scoreText.update);
			GameMgr.scoreText = scoreText;

			var trapGenerator = new TrapGenerator(function(trap) {
				trap.y = GameMgr.cooHeight * GameMgr.scaleFactor + Math.random() * 200;
				trap.updateId = GameMgr.gameTicker.addUpdate(trap.update);
				GameMgr.stage.addChild(trap);
			});
			trapGenerator.updateId = GameMgr.gameTicker.addUpdate(trapGenerator.update);
			GameMgr.trapGenerator = trapGenerator;
		});

	}

	function run() {
		createjs.Ticker.on("tick", GameMgr.gameTicker.tick);
		createjs.Ticker.framerate = 60;
	}

	return {
		init: init,
		initGameObject: initGameObject,
		run: run
	};
});
